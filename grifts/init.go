package grifts

import (
	"cm/cara/cara_buffalo_part3/my_app/actions"

	"github.com/gobuffalo/buffalo"
)

func init() {
	buffalo.Grifts(actions.App())
}

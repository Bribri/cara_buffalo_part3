# PARTIE 3 - Login et Middleware

Maintenant que l'utilisateur peut s'enregistrer, on va gérer le Login et le Logout.

## 3.1 - Login & Logout

**Login**


En premier lieu, créez un fichier `login.html` sous `/templates/users` et collez-y le bloc ci-dessous :

```
<div class="row mt-3 justify-content-center">
    <div class="col-lg-6 col-md-8 col-sm-10">
        <div class="card">
            <div class="card-header">
                <h3>Login</h3>
            </div>
            <div class="card-body">
                <%= if (errors) { %>
                <%= errors %>
                <%= for (key, val) in errors { %>
                    <div class="alert alert-danger alert-dismissible fade show m-1" role="alert">
                        <%= val %>
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                <% } %>
                <% } %>

                <!--TODO : mapper le formulaire avec la route de login-->

                <form action="#" method="POST" novalidate>
                    <%= csrf() %>
                    <div class="form-group">
                        <label for="email">Email address</label>
                        <input type="email" name="Email" class="form-control" id="email">
                    </div>
                    <div class="form-group">
                        <label for="pwd">Password</label>
                        <input name="Password" type="password" class="form-control" id="pwd">
                    </div>
                    <button type="submit" class="btn btn-primary btn-block">Login</button>
                </form>
            </div>
        </div>
    </div>
</div>
```


On peut créer un utilisateur, mais il ne peut pas encore se logger. Il va donc falloir :

1. Compléter le controller en implémentant le Handler LoginGet(). Sa seule fonction est de renvoyer vers le formulaire de login.

2. Créer une méthode Authorize() dans `models/users.go`qui vérifie que l'email du user existe bien en BDD

3. Compléter le controller en implémentant le Handler LoginPost().
    - créer un user vide
    - binder ce user au context
    - vérifier si l'utilisateur est autorisé, si non le renvoyer vers la page de login
    - si oui, le binder à la session
    - envoyer un petit flash pour lui dire qu'on est content de le revoir (cf. doc Buffalo sur les Flash)
    - le rediriger vers la page d'accueil

4. Ajouter les mappings routes <-> handler dans app.go

5. Ajouter la route vers le login dans les templates (application.html & login.html)


**Logout**

Si vous avez réussi la gestion du Login, vous devriez vous en sortir facilement pour le Logout (implémentation d'un Handler Logout()). Pour l'instant, les boutons Login et Logout sont visibles que l'utilisateur soit connecté ou non.


## 3.2 Un middleware pour la route

Nous allons créer un petit middleware qui va être exécuté entre l'appel à la route loginPath() et l'exécution du handler LoginPost(). Ce middleware va stocker l'id de l'utilisateur dans la Session et nous permettre de filtrer dans les templates les boutons affichés (Login si aucun utilisateur courant, Logout autrement).

1. Créer un fichier `middleware.go` sous le répertoire `actions`

2. Y écrire la fonction `SetCurrentUser` qui prendra en paramètre un buffalo.Handler (cf. https://gobuffalo.io/en/docs/middleware )

3. Déclarer ce middleware dans `app.go`

4. Modifier les templates pour n'afficher le bouton Login que lorsqu'aucun utilisateur n'est loggé, Logout sinon